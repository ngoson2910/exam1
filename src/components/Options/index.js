import { useState } from 'react'
import './style.css'

const options = [
  'Python',
  'Javascript',
  'Php',
  'C#'
]

function Options() {
  const [checked, setChecked] = useState();

  return (
    
    <div>
      {options.map((opt, index) => (
        <div className="form-group" key={index}>
          <input 
            type="radio" 
            id={`option-${index + 1}`}  
            checked={checked === opt}
            value={opt}
            onChange={e => setChecked(e.target.value)}
          />
          <label htmlFor={`option-${index + 1}`}>{opt}</label>
        </div>
      ))}
    </div>
  )
}

export default Options