import Header from "./components/Header";
import Options from "./components/Options";
import './App.css'

function App() {
  return (
    <div className="App">
      <Header />
      <Options />
    </div>
  );
}

export default App;
